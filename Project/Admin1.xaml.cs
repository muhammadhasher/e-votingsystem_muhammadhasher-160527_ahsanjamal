﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace Project
{
    /// <summary>
    /// Interaction logic for Admin1.xaml
    /// </summary>
    public partial class Admin1 : Page
    {

        public string ename;
        public string location;
        public string position;
        public string cand1;
        public string party1;
        public string cand2;
        public string party2;
        public int vote1, vote2;
        bool Condition;


        public Admin1()
        {
            InitializeComponent();
        }

        private void SaveButton(object sender, RoutedEventArgs e)
        {
            Condition = true;
            String input = ((TextBox)grid.FindName("elname")).Text;
            if (input.Equals(""))
            {
                MessageBox.Show("Error: Name is required!");
                Condition = false;
            }
            else
            {
                ename = input;
            }

            input = ((TextBox)grid.FindName("locat")).Text;
            if (input.Equals(""))
            {
                MessageBox.Show("Error: Location is required!");
                Condition = false;
            }
            else
            {
                location = input;
            }


            input = ((TextBox)grid.FindName("posit")).Text;
            if (input.Equals(""))
            {
                MessageBox.Show("Error: Position is required!");
                Condition = false;
            }
            else
            {
                position = input;
            }


            input = ((TextBox)grid.FindName("candit1")).Text;
            if (input.Equals(""))
            {
                MessageBox.Show("Error: Candidate 1 is required!");
                Condition = false;
            }
            else
            {
                cand1 = input;
            }

            input = ((TextBox)grid.FindName("candit2")).Text;
            if (input.Equals(""))
            {
                MessageBox.Show("Error: Candidate 2 is required!");
                Condition = false;
            }
            else
            {
                cand2 = input;
            }

            input = ((ComboBox)grid.FindName("par1")).Text;       

            if (input.Equals(""))
            {
                Condition = false;
                MessageBox.Show("Candidates 1 party must be provided!");
            }
            else
            {
                party1 = input;
            }

            input = ((ComboBox)grid.FindName("par2")).Text;       

            if (input.Equals(""))
            {
                Condition = false;
                MessageBox.Show("Candidates 2 party must be provided!");
            }
            else
            {
                party2 = input;
            }

            string jaaniKiString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\DELL\source\repos\Project\Project\Database1.mdf;Integrated Security=True";
            SqlConnection jaaniKaConnection = new SqlConnection(jaaniKiString);
            

            //string jaaniKiQuery = "INSERT INTO ElectionDetails(ElectionName,Location,Position,Candidate1,Party1,Candidate2,Party2,Vote1,Vote2)VALUES(@a,@b,@c,@d,@e,@f,@g,@h,@i)";


           
            SqlCommand jaaniKiCommand = new SqlCommand();
            jaaniKiCommand.Connection = jaaniKaConnection;
            jaaniKiCommand.CommandText = "INSERT INTO ElectionDetails(ElectionName,Location,Postion,Candidate1,Party1,Candidate2,Party2,Vote1,Vote2)VALUES(@a,@b,@c,@d,@e,@f,@g,@h,@i)";

            jaaniKiCommand.Parameters.AddWithValue("@a", ename);
            jaaniKiCommand.Parameters.AddWithValue("@b", location);
            jaaniKiCommand.Parameters.AddWithValue("@c", position);
            jaaniKiCommand.Parameters.AddWithValue("@d", cand1);
            jaaniKiCommand.Parameters.AddWithValue("@e", party1);
            jaaniKiCommand.Parameters.AddWithValue("@f", cand2);
            jaaniKiCommand.Parameters.AddWithValue("@g", party2);
            jaaniKiCommand.Parameters.AddWithValue("@h", 0);
            jaaniKiCommand.Parameters.AddWithValue("@i", 0);

            jaaniKaConnection.Open();
             jaaniKiCommand.ExecuteNonQuery();
           
            
            jaaniKaConnection.Close();
            MessageBox.Show("Data Saved");
            //Admin2 page_object = new Admin2(ename,location,position,cand1,cand2,party1,party2,vote1,vote2);
            //this.NavigationService.Navigate(page_object);

        }

        private void ShowAll(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri("Admin2.xaml", UriKind.Relative);
            this.NavigationService.Navigate(uri);

        }




    }
}

