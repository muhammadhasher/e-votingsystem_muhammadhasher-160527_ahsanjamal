﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Page
    {
        public AdminWindow()
        {
            InitializeComponent();
        }

        

        public void Admin1(object sender,RoutedEventArgs e)
        {
            String username = "admin";
            String password = "admin";

            String input = ((TextBox)grid.FindName("username")).Text;
            String input2 = ((TextBox)grid.FindName("pass")).Text;

            if (input.Equals(username) && input2.Equals(password))
            {
                Uri uri = new Uri("Admin1.xaml", UriKind.Relative);
                this.NavigationService.Navigate(uri);
            }
            else
            {
                MessageBox.Show("Username or Password are invalid");
            }



        }
    }
}
