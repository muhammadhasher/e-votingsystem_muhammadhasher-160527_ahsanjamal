﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Project
{
    class Election_Details : INotifyPropertyChanged
    {

        static Election_Details()
        {
            createTable();
        }



        public string ElectionName { get; set; }
        public string Location { get; set; }
        public string Position { get; set; }
        public string Candidate1 { get; set; }
        public string Party1 { get; set; }
        public string Candidate2 { get; set; }
        public string Party2 { get; set; }
        public int Vote1 { get; set; }
        public int Vote2 { get; set; }

        public ObservableCollection<Election_Details> ed = new ObservableCollection<Election_Details>();

        public event PropertyChangedEventHandler PropertyChanged;

        public void save(string elec_name, string loc, string pos, string cand1, string par1, string cand2, string par2, int v1, int v2)
        {
            this.ElectionName = elec_name;
            this.Location = loc;
            this.Position = pos;
            this.Candidate1 = cand1;
            this.Party1 = par1;
            this.Candidate2 = cand2;
            this.Party2 = par2;
            this.Vote1 = v1;
            this.Vote2 = v2;
            ed.Add(this);
        }





        public static DataTable InfoTable = new DataTable();

        public static void createTable()
        {
            InfoTable.Columns.Add("ElectionName", typeof(string));
            InfoTable.Columns.Add("Location", typeof(string));
            InfoTable.Columns.Add("Position", typeof(string));
            InfoTable.Columns.Add("Candidate1", typeof(string));
            InfoTable.Columns.Add("Party1", typeof(string));
            InfoTable.Columns.Add("Candidate2", typeof(string));
            InfoTable.Columns.Add("Party2", typeof(string));
            InfoTable.Columns.Add("Vote1", typeof(int));
            InfoTable.Columns.Add("Vote2", typeof(int));

            InfoTable.AcceptChanges();
            InfoTable.RowChanged += new DataRowChangeEventHandler(Row_Changed);
        }


        public void addToTable(string elec_name, string loc, string pos, string cand1, string par1, string cand2, string par2, int v1, int v2)
        {
            InfoTable.Rows.Add(elec_name,loc,pos,cand1,par1,cand2,par2,v1,v2);
            //  updateDB();
        }


        private static void Row_Changed(object sender, DataRowChangeEventArgs e)
        {
            string customerDetails = "";
            foreach (DataRow r in InfoTable.Rows)
            {
                customerDetails += r["ElectionName"] + " " + r["Location"] + " " + r["Position"] + " " + r["Candidate1"] + " " + r["Party1"] + " " + r["Candidate2"] + " " + r["Party2"] + " " + r["Vote1"] + " " + r["Vote2"] + "\n";
            }
            //  updateDB();
        }

        /*   public static void updateDB()
           {
               SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);
               sda.Update(InfoTable);
           }
   */

        public static SqlDataAdapter sda;
        static int y = 0;

        public static void getData()

        {       
            try
            {
                DB.conn.Open();
                
                string query = "SELECT * FROM ElectionDetails";
                SqlCommand command = new SqlCommand(query, DB.conn);
                sda = new SqlDataAdapter(command);
                sda.Fill(InfoTable);
                DB.conn.Close();
                InfoTable.AcceptChanges();
                 
                InfoTable.RowChanged += new DataRowChangeEventHandler(Row_Changed);
                

            }
            catch(System.Data.SqlClient.SqlException sql)
            {
                MessageBox.Show(sql.Message);
            }

        }



        public static void getData(string ename, string location_, string position_, string cand1_, string cand2_, string party1_, string party2, int vo1, int vo2)
        {
            try
            {
                string query = "SELECT * FROM ElectionDetails";
                SqlCommand command = new SqlCommand(query, DB.conn);
                sda = new SqlDataAdapter(command);
                sda.Fill(InfoTable);
                InfoTable.AcceptChanges();

                InfoTable.RowChanged += new DataRowChangeEventHandler(Row_Changed);

            }
            catch (System.Data.SqlClient.SqlException sql)
            {
                MessageBox.Show(sql.Message);
            }

        }






    }
}
