﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Page
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void NavigateToAdmin(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri("AdminWindow.xaml", UriKind.Relative);
            this.NavigationService.Navigate(uri);
        }
        public void NavigateToVoter(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri("VoterWindow.xaml", UriKind.Relative);
            this.NavigationService.Navigate(uri);
        }


    }
}
